package lab2.dc.unifil.br;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

public class Classificadores <T> {

    /**
     * Define valor que o mergesort testa a condição para
     * enviar o restante da lista para ser tratada pelo Insertion Sort
     * @param threshold valor de elementos na lista a ser ordenado pelo insertion sort.
     * Atenção, se o valor for muito alto, o Insertion sort tem um desempenho n².
     */
    public static void setK(int threshold){
        k = threshold;
    }


    /**
     * Classifica a lista em ordem crescente, pelo método de
     * inserção, in-place.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     */
                                            //big O (n^2)
                                            //Omega (n)
    public static void insertionsort(List<Integer> lista) {
        for (int i = 1; i < lista.size(); i++) {
            Integer elem = lista.get(i);

            int j = i;
            while (j > 0 && lista.get(j - 1) > elem) {
                lista.set(j, lista.get(j - 1));
                j--;
            }

            lista.set(j, elem);
        }
    }

    /**
     * Classifica a lista em ordem crescente, pelo método de
     * selection sort, in-place.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     */
                                            //Theta Θ(n^2)
    public static void selectionsort(List<Integer> lista) {
        for (int i = 0; i < lista.size(); i++) {
            int menorIdx = encontrarIndiceMenorElem(lista, i);
            permutar(lista, menorIdx, i);
        }
    }


    /**
     * Classifica a lista em ordem crescente, pelo método de
     * mergesort, in-place. Utiliza memória auxiliar.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     */
    public static void mergesort(List<Integer> lista) {
        if(lista.size() <= k){
            insertionsort(lista);
        }else{

            final int idxMeioLista = lista.size() / 2;
            List<Integer> esquerda = lista.subList(0, idxMeioLista);
            mergesort(esquerda);

            List<Integer> direita = lista.subList(idxMeioLista, lista.size());
            mergesort(direita);

            merge(lista, esquerda, direita);
        }
    }

    /**
     * Classifica a lista em ordem crescente, pelo método de
     * bubblesort, in-place.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     * @param <Modelo> Função de comparação para correta ordenação.
     */
                                                            // complexidade: O (n²),  Omega(n)
    public static <Modelo extends Comparable<Modelo>> void bubblesort(List<Modelo> lista) {
        bubblesort(lista, (l, r) -> l.compareTo(r));
    }



    /**
     * Quicksort
     * Separa a lista em 2 partes, ordenação separada e depois combinadas.
     * @param lista Lista a ser classificada, sofre mutação (in-place).
     */
    // Θ (n logn)
    public static void quicksort(List<Integer> lista) { quicksort(lista, (vs) -> vs.get(rng.nextInt(vs.size())) ); }
    // O (n²) Omega(n logn)
    public static void hoares(List<Integer> lista) { hoares(lista, (vs) -> vs.get(rng.nextInt(vs.size())) );    }
    // O (n²) Omega(n logn)
    public static void lomuto(List<Integer> lista) { lomuto(lista, (vs) -> vs.get(rng.nextInt(vs.size())) ); }


    // ********* METODOS PRIVADOS ********* //
    private static void quicksort(List<Integer> lista, Function<List<Integer>, Integer> escolhedorPivo) {
        if (lista.size() <= 1) return; //1


        Integer pivo = escolhedorPivo.apply(lista); //1
        int idxPivo = reorganizar(lista, pivo, (l,r) -> l.compareTo(r));

        quicksort(lista.subList(0, idxPivo));
        quicksort(lista.subList(idxPivo+1, lista.size()));
    }
    private static void lomuto(List<Integer> lista,  Function<List<Integer>, Integer> escolhedorPivo) {
        if (lista.size() <= 1) return;

        Integer pivo = escolhedorPivo.apply(lista);
        int idxPivo = lomutoPartition(lista, pivo,  (l,r) -> l.compareTo(r));

        lomuto(lista.subList(0, idxPivo));
        lomuto(lista.subList(idxPivo+1, lista.size()));
    }
    private static void hoares(List<Integer> lista,  Function<List<Integer>, Integer> escolhedorPivo) {
        // Caso base
        if (lista.size() <= 1) return;

        Integer pivo = escolhedorPivo.apply(lista);
        int idxPivo = reorganizar(lista, pivo,  (l,r) -> l.compareTo(r));

        lomuto(lista.subList(0, idxPivo));
        lomuto(lista.subList(idxPivo+1, lista.size()));
    }
    private static int lomutoPartition(List<Integer> lista, Integer pivo, Comparator<Integer> cmp) {
        int l = 0;
        for (int r = 0; r < lista.size()-1; r++) {
            if( cmp.compare(lista.get(r), pivo) <= 0){
                l++; permutar(lista, r, l);
            }
        }

        permutar(lista, l, lista.size()-1);
        return l;
    }

    private static int reorganizar(List<Integer> lista, Integer pivo, Comparator<Integer> cmp) {
        int l = 0, r = lista.size() - 1; //2
        while (true) {
            while (cmp.compare(lista.get(l), pivo) < 0) { //n log +2
                l++;
            }
            while (cmp.compare(lista.get(r), pivo) > 0) { // n log +2
                r--;
            }

            if (l < r) {
                permutar(lista, l, r);
            } else return l; //3
        }
    }
    private static int encontrarIndiceMenorElem(List<Integer> lista, int idxInicio) {
        int menor = idxInicio;
        for (int i = idxInicio+1; i < lista.size(); i++) {
            if (lista.get(menor) > lista.get(i))  menor = i;
        }
        return menor;
    }

    private static void merge(List<Integer> lista, List<Integer> esquerda, List<Integer> direita) {
        List<Integer> merged = new ArrayList<>(lista);

        int idxE = 0, idxD = 0, idxL = 0;
        while (idxE < esquerda.size() && idxD < direita.size()) {
            if (esquerda.get(idxE) < direita.get(idxD)) {
                merged.set(idxL, esquerda.get(idxE));
                idxE++;
            } else {
                merged.set(idxL, direita.get(idxD));
                idxD++;
            }
            idxL++;
        }

        int idxF;
        List<Integer> faltantes;
        if (idxE < esquerda.size()) {
            faltantes = esquerda;
            idxF = idxE;
        } else {
            faltantes = direita;
            idxF = idxD;
        }

        while (idxF < faltantes.size()) {
            merged.set(idxL, faltantes.get(idxF));
            idxL++; idxF++;
        }

        for (int i = 0; i < lista.size(); i++) {
            lista.set(i, merged.get(i));
        }
    }
    private static void permutar(List<Integer> lista, int a, int b) {
        Integer permutador = lista.get(a);
        lista.set(a, lista.get(b));
        lista.set(b, permutador);
    }
    private static <TElem> void bubblesort(List<TElem> lista, Comparator<TElem> comparador) {
        boolean houvePermuta; do {
            houvePermuta = false;

            // Sobe a bolha
            for (int i = 1; i < lista.size(); i++) {

                if (comparador.compare(lista.get(i - 1), lista.get(i)) > 0) {
                    permutar((List<Integer>) lista, i - 1, i);
                    houvePermuta = true;
                }
            }
        } while (houvePermuta);
    }

    private static boolean isOrdenada(List<Integer> lista) {
        for (int i = 1; i < lista.size(); i++)
            if (lista.get(i - 1) > lista.get(i))
                return false;

        return true;
    }

    private static Random rng = new Random("Seed constante repetível".hashCode());
    private static int k = 1 ;
}
