package lab2.dc.unifil.br;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FabricaListas {

    /**
     * Cria uma lista de integers crescente, com quantidade de elementos especificável.
     * @param qtdeElems Quantidade de elementos da lista a ser gerada.
     * @return Uma lista cuja única característica pré-definida é ter elementos classificados
     * crescentemente.
     */
    public static List<Integer> fabricarListaIntegersCrescente(int qtdeElems) {

        int maiorvalor = 65536;
        int passo = 256;

        List<Integer> elemAleatorios= new ArrayList<>(qtdeElems);
        while(qtdeElems--  > 0) {
            int rng = rngPadrao.nextInt(passo);
            if( rng != 0 ){
                maiorvalor += rng;
                elemAleatorios.add(maiorvalor);
            }
        }
        return elemAleatorios;
    }


    /**
     * Cria uma lista de integers decrescente, com quantidade de elementos especificável.
     * @param qtdeElems Quantidade de elementos da lista a ser gerada.
     * @return Uma lista cuja única característica pré-definida é ter elementos classificados
     * decrescentemente.
     */
    public static List<Integer> fabricarListaIntegersDecrescente(int qtdeElems) {
        int menorvalor = 65536;
        int passo = 256;

        List<Integer> elemAleatorios= new ArrayList<>(qtdeElems);
        while(qtdeElems--  > 0) {
            int rng = rngPadrao.nextInt(passo);
            if(rng != 0 ){
                menorvalor = menorvalor - rng;
                elemAleatorios.add(menorvalor);
            }
        }
        return elemAleatorios;
    }

    /**
     * Cria uma lista de integers, com quantidade de elementos especificável.
     * @param qtdeElems Quantidade de elementos da lista a ser gerada.
     * @return Uma lista com elementos aleatórios e tamanho pré-definido.
     */
    public static List<Integer> fabricarListaIntegersAleatoria(int qtdeElems) {
        return fabricarListaIntegersAleatoria(qtdeElems, rngPadrao);

    }

    /**
     * Cria uma lista de integers, com quantidade de elementos especificável.
     * @param qtdeElems Quantidade de elementos da lista a ser gerada.
     * @param rng Gerador de números aleatórios a ser utilizado para construir a lista.
     * @return Uma lista com elementos aleatórios e tamanho pré-definido.
     */
    public static List<Integer> fabricarListaIntegersAleatoria(int qtdeElems, Random rng) {
        List<Integer> elemAleatorios= new ArrayList<>(qtdeElems);
        while(qtdeElems-- > 0) {
            elemAleatorios.add(rng.nextInt());
        }
        return elemAleatorios;
    }



    private static Random rngPadrao = new Random("Pipeline de integers.".hashCode());

    /**
     * Ninguém pode construir instâncias dessa classe, que é meramente um conjunto
     * de utilidade de listas.
     */
    private FabricaListas() {}
}
