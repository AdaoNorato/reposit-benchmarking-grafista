package lab2.dc.unifil.br;
import java.util.List;

public class Main {

    enum Testes{
            teste1, //Comparação entre seleção, bolha e inserção com listas crescentes.
            teste2, //Comparação entre seleção, bolha e inserção com listas decrescentes.
            teste3, //Comparação entre inserção e mergesort com listas crescentes.
            teste4, //Comparação entre inserção e mergesort com listas decrescentes.
            teste5, //Comparação entre mergesort e quicksort com listas decrescentes.
            teste6, //Comparação entre mergesort e quicksort com listas crescentes.
            teste7, //Exercicio mergesort com threshold que ordena com insertion se lista < k .
            teste8  //Hoare e Lomuto comparação . *
    }

    //altere o teste(n) que você quer executar aqui.
    static Testes run = Testes.teste1;

    public static void main(String[] args) {
        StartBenchmark(run);
    }

    private static void StartBenchmark(Testes run){
        switch (run){
            case teste1:  TesteListaCrescenteSelBubIns();  break;
            case teste2:  TesteListaDecresenteSelBubIns(); break;
            case teste3:  TesteLista_Cresce_Ins_Merge();   break;
            case teste4:  TesteLista_Decre_Ins_Merge();    break;
            case teste5:  TesteLista_Decre_Merge_Quick();  break;
            case teste6:  TesteLista_Cresce_Merge_Quick(); break;
            case teste7:  TesteLista_Merge_Com_Insert(); break;
            case teste8:  TesteLista_Lomuto(); break;
        }
    }

    private static void TesteListaCrescenteSelBubIns(){
        // Realiza benchmarkings
        List<Medicao> medicoesSelectionSort = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::selectionsort);

        List<Medicao> medicaoBubbleSort = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::bubblesort);

        List<Medicao> medicaoInsertionSort = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::insertionsort);


        TabelaTempos tt = baseTabela();
        tt.setLegendas("Selection", "Bubble","Insertion");
        for (int i = 0; i < medicoesSelectionSort.size(); i++) {
            Medicao amostraSelection = medicoesSelectionSort.get(i);
            Medicao amostraBubble = medicaoBubbleSort.get(i);
            Medicao amostraInsertion = medicaoInsertionSort.get(i);

            tt.anotarAmostra(amostraSelection.getN(),
                    amostraSelection.getTempoSegundos(), amostraBubble.getTempoSegundos(),amostraInsertion.getTempoSegundos());
            }

        tt.exibirGraficoXY();
    }    // teste 1
    private static void TesteListaDecresenteSelBubIns(){
        // Realiza benchmarkings
        List<Medicao> medicoesSelectionSort = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersDecrescente, Classificadores::selectionsort);

        List<Medicao> medicaoBubbleSort = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersDecrescente, Classificadores::bubblesort);

        List<Medicao> medicaoInsertionSort = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersDecrescente, Classificadores::insertionsort);


        TabelaTempos tt = baseTabela();
        tt.setLegendas("Selection", "Bubble","Insertion");
        for (int i = 0; i < medicoesSelectionSort.size(); i++) {
            Medicao amostraSelection = medicoesSelectionSort.get(i);
            Medicao amostraBubble = medicaoBubbleSort.get(i);
            Medicao amostraInsertion = medicaoInsertionSort.get(i);

            tt.anotarAmostra(amostraSelection.getN(),
                    amostraSelection.getTempoSegundos(), amostraBubble.getTempoSegundos(),amostraInsertion.getTempoSegundos());
        }

        tt.exibirGraficoXY();
    }   // teste 2
    private static void TesteLista_Cresce_Ins_Merge(){
        // Realiza benchmarkings
        List<Medicao> medicaoInsertion = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::insertionsort);

        List<Medicao> medicaoMerge = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::mergesort);


        TabelaTempos tt = baseTabela();
        tt.setLegendas("InsertionSort", "Mergesort");
        for (int i = 0; i < medicaoInsertion.size(); i++) {
            Medicao amostraInsertion = medicaoInsertion.get(i);
            Medicao amostraMerge = medicaoMerge.get(i);

            tt.anotarAmostra(amostraInsertion.getN(),
            amostraInsertion.getTempoSegundos(), amostraMerge.getTempoSegundos());
        }

        tt.exibirGraficoXY();
    }     // teste 3
    private static void TesteLista_Decre_Ins_Merge(){
        List<Medicao> medicaoInsertion = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
        FabricaListas::fabricarListaIntegersDecrescente, Classificadores::insertionsort);

        List<Medicao> medicaoMerge = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
        FabricaListas::fabricarListaIntegersDecrescente, Classificadores::mergesort);

        TabelaTempos tt = baseTabela();
        tt.setLegendas("InsertionSort","Mergesort");
        for (int i = 0; i < medicaoInsertion.size(); i++) {
            Medicao amostraInsertion = medicaoInsertion.get(i);
            Medicao amostraMerge = medicaoMerge.get(i);

            tt.anotarAmostra(amostraInsertion.getN(),
                    amostraInsertion.getTempoSegundos(), amostraMerge.getTempoSegundos());
        }

        tt.exibirGraficoXY();
    }      // teste 4
    private static void TesteLista_Decre_Merge_Quick(){
        // Realiza benchmarkings
        List<Medicao> medicaoQuick = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersDecrescente, Classificadores::quicksort);

        List<Medicao> medicaoMerge = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersDecrescente, Classificadores::mergesort);



        // Plotta gráfico com resultados levantados
        TabelaTempos tt = baseTabela();
        tt.setLegendas("Quicksort","MergeSort");
        for (int i = 0; i < medicaoQuick.size(); i++) {
            Medicao amostraQuick = medicaoQuick.get(i);
            Medicao amostraMerge = medicaoMerge.get(i);

            tt.anotarAmostra(amostraQuick.getN(),
                    amostraQuick.getTempoSegundos(), amostraMerge.getTempoSegundos());
        }

        tt.exibirGraficoXY();
    }    // teste 5
    private static void TesteLista_Cresce_Merge_Quick(){
        // Realiza benchmarkings
        List<Medicao> medicaoQuick = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::quicksort);

        List<Medicao> medicaoMerge = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::mergesort);



        // Plotta gráfico com resultados levantados
        TabelaTempos tt = baseTabela();
        tt.setLegendas("Quicksort","Mergesort");
        for (int i = 0; i < medicaoQuick.size(); i++) {
            Medicao amostraQuick = medicaoQuick.get(i);
            Medicao amostraMerge = medicaoMerge.get(i);

            tt.anotarAmostra(amostraQuick.getN(),
                    amostraQuick.getTempoSegundos(), amostraMerge.getTempoSegundos());
        }

        tt.exibirGraficoXY();
    }   // teste 6
    private static void TesteLista_Merge_Com_Insert(){
        Classificadores threshold = new Classificadores();

        threshold.setK(1);
        List<Medicao> medicao1 = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::mergesort);

        threshold.setK(2);
        List<Medicao> medicao2 = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::mergesort);

        threshold.setK(8);
        List<Medicao> medicao8 = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::mergesort);

        threshold.setK(16);
        List<Medicao> medicao16 = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::mergesort);

        threshold.setK(32);
        List<Medicao> medicao32 = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::mergesort);

        TabelaTempos tt = baseTabela();
        tt.setLegendas("K=1","K=2", "K=8", "K=16", "K=32");
        for (int i = 0; i < medicao1.size(); i++) {
            Medicao amostraK1 = medicao1.get(i);
            Medicao amostraK2 = medicao2.get(i);
            Medicao amostraK8 = medicao8.get(i);
            Medicao amostraK16 = medicao16.get(i);
            Medicao amostraK32 = medicao32.get(i);

            tt.anotarAmostra(amostraK1.getN(),
                    amostraK1.getTempoSegundos(),
                    amostraK2.getTempoSegundos(),
                    amostraK8.getTempoSegundos(),
                    amostraK16.getTempoSegundos(),
                    amostraK32.getTempoSegundos());
        }

        tt.exibirGraficoXY();
    }     // teste 7
    private  static void TesteLista_Lomuto(){
        List<Medicao> medicaoLomuto = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::lomuto);

        List<Medicao> medicaoHoares = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso, repeticao,
                FabricaListas::fabricarListaIntegersCrescente, Classificadores::hoares);

        // Plotta gráfico com resultados levantados
        TabelaTempos tt = baseTabela();
        tt.setLegendas("Lomuto", "Hoares");
        for (int i = 0; i < medicaoLomuto.size(); i++) {
            Medicao amostraLomuto = medicaoLomuto.get(i);
            Medicao amostraHoares = medicaoHoares.get(i);

            tt.anotarAmostra(amostraLomuto.getN(), amostraLomuto.getTempoSegundos(), amostraHoares.getTempoSegundos());
        }

        tt.exibirGraficoXY();
    }              // teste 8

    private static TabelaTempos baseTabela(){
        TabelaTempos tt = new TabelaTempos();
        tt.setTitulo("Tempo para ordenação");
        tt.setEtiquetaX("Qtde elementos lista");
        tt.setEtiquetaY("Tempo (ms)");
        return tt;
    }

    static int nInicial  = 0;
    static int nFinal    = 100;
    static int nPasso    = 1;
    static int repeticao = 100;

}
